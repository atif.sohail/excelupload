﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExcelTask.Models;

namespace ExcelTask.Controllers
{
    public class StudentClass
    {
        public string[] FullName { get; set; }
        public string[] Affiliate { get; set; }
        public string[] Sponsor { get; set; }
        public string[] FullNameAr { get; set; }
        public string[] Gender { get; set; }
        public string[] Nationality { get; set; }
        public string[] EmiratesId { get; set; }
        public DateTime?[] BirthDate { get; set; }
        public string[] Marital { get; set; }
        public string[] MobileNum { get; set; }
        public string[] Email { get; set; }
        public string[] LearnerStatus { get; set; }
        public string[] UserId { get; set; }
    }
    public class AdmissionClass
    {
        public string[] Admission { get; set; }
        public string[] Class { get; set; }
        public string[] Offer { get; set; }
        public string[] Eligibility { get; set; }
        public string[] PrimaryMobileNum { get; set; }
        public string[] FullName { get; set; }
        public string[] Gender { get; set; }
        public string[] Nationality { get; set; }
        public string[] EmiratesId { get; set; }
        public DateTime?[] BirthDate { get; set; }
        public DateTime?[] ApplyDate { get; set; }
        public DateTime?[] SubmitDate { get; set; }
        public DateTime?[] RegisterDate { get; set; }
        public DateTime?[] ActivationDate { get; set; }
        public string[] Marital { get; set; }
        public string[] MobileNum { get; set; }
        public string[] Email { get; set; }
    }
    public class ExcelImportController : Controller
    {
        // GET: ExcelImport
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UploadExcel(HttpPostedFileBase file)
        {
            try
            {
                //Checking file content length and Extension must be .xlsx  
                if (file != null && file.ContentLength > 0 && Path.GetExtension(file.FileName).ToLower() == ".xlsx")
                {
                    if (!Directory.Exists(Server.MapPath("~/Content/Attachments/temp")))
                        Directory.CreateDirectory(Server.MapPath("~/Content/Attachments/temp"));
                    string path = Path.Combine(Server.MapPath("~/Content/Attachments/temp"), Path.GetFileName(file.FileName));
                    //Saving the file  
                    file.SaveAs(path);
                }
            }
            catch (Exception ee)
            {
                TempData["Error"] = "File format is not acceptable!";
            }
            return Json("File Upload Successfully");
        }
        public ActionResult GetExcelData(string path)
        {
            DataTable dt = new DataTable();
            try
            {
                path = Path.Combine(Server.MapPath("~/Content/Attachments/temp"), path);
                dt = ReadExcelFile(path);
                if (dt.Columns.Count == 13)
                    return View("Learner", dt);
                else
                    return View("Addmission", dt);

            }
            catch (Exception ee)
            {
            }
            return View("Index", dt);

        }
        [HttpPost]
        public DataTable ReadExcelFile(string path)
        {
            DataTable dt = new DataTable();
            //Started reading the Excel file.  
            using (XLWorkbook workbook = new XLWorkbook(path))
            {
                IXLWorksheet worksheet = workbook.Worksheet(1);
                bool FirstRow = true;
                //Range for reading the cells based on the last cell used.  
                string readRange = "1:1";
                foreach (IXLRow row in worksheet.RowsUsed())
                {
                    //If Reading the First Row (used) then add them as column name  
                    if (FirstRow)
                    {
                        //Checking the Last cellused for column generation in datatable  
                        readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                        foreach (IXLCell cell in row.Cells(readRange))
                        {
                            dt.Columns.Add(cell.Value.ToString());
                        }
                        FirstRow = false;
                    }
                    else
                    {
                        //Adding a Row in datatable  
                        dt.Rows.Add();
                        int cellIndex = 0;
                        //Updating the values of datatable  
                        foreach (IXLCell cell in row.Cells(readRange))
                        {
                            dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                            cellIndex++;
                        }
                    }
                }
                //If no data in Excel file  
                if (FirstRow)
                    ViewBag.Message = "Empty Excel File!";
            }
            // If file found, delete it 
            if (System.IO.File.Exists(path))   
                System.IO.File.Delete(path);
            return dt;
        }
        [HttpPost]
        public ActionResult UploadStudentData(StudentClass param)
        {
            Models.ExcelTask db = new Models.ExcelTask();
            try
            {
                string missingMsg = "These user Email or Emirate Id is missing ";
                for (int i = 0; i < param.FullName.Length; i++)
                {
                    sp_StudentInfo_Post_Result model = db.sp_StudentInfo_Post(param.FullName[i], param.Affiliate[i], param.Sponsor[i], param.FullNameAr[i],
                        param.Gender[i], param.Nationality[i], param.EmiratesId[i], param.BirthDate[i], param.Marital[i], param.MobileNum[i], param.Email[i], param.LearnerStatus[i],
                        param.UserId[i]).FirstOrDefault();
                    if(model.Message == "Emirates ID and Email cannot be null")
                    {
                        missingMsg = missingMsg + param.FullName[i] + ", ";
                        TempData["Message"] = missingMsg;
                    }
                }
                if (TempData["Message"] == null)
                    TempData["Success"] = "Data Uploaded Successfully!";
            }
            catch (Exception ee)
            {
                TempData["Error"] = "Execution Time Error: " + ee.Message;
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult UploadAdmissionData(AdmissionClass param)
        {
            Models.ExcelTask db = new Models.ExcelTask();
            try
            {
                string missingMsg = "These user Email or Emirate Id is missing ";
                for (int i = 0; i < param.Admission.Length; i++)
                {
                    sp_AdmissionInfo_Post_Result model = db.sp_AdmissionInfo_Post(param.FullName[i], param.Gender[i], param.Nationality[i], param.EmiratesId[i],
                        param.BirthDate[i], param.Marital[i], param.MobileNum[i], param.Email[i], param.Admission[i], param.Class[i],
                        param.ApplyDate[i], param.SubmitDate[i], param.Offer[i], param.Eligibility[i], param.RegisterDate[i], param.ActivationDate[i],
                        param.PrimaryMobileNum[i]).FirstOrDefault();
                    if (model.Message == "Emirates ID and Email cannot be null")
                    {
                        missingMsg = missingMsg + param.FullName[i] + ", ";
                        TempData["Message"] = missingMsg;
                    }
                }
                if (TempData["Message"] == null)
                    TempData["Success"] = "Data Uploaded Successfully!";
            }
            catch (Exception ee)
            {
                TempData["Error"] = "Execution Time Error: " + ee.InnerException;
            }
            return RedirectToAction("Index");
        }
    }
}
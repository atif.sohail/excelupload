// Class definition
var KTFormControls = function () {
	// Private functions
	var _initDemo1 = function () {
		FormValidation.formValidation(
            document.getElementById('projectForm'),
			{
                fields: {
                    Title: {
                        validators: {
                            notEmpty: {
                                message: 'Title is required'
                            },
                            regexp: {
                                regexp: /^[a-z\d\-_\s]+$/i,
                                message: 'The title name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    Description: {
                        validators: {
                            notEmpty: {
                                message: 'Description is required'
                            },
                            regexp: {
                                regexp: /^[a-z\d\-_\s]+$/i,
                                message: 'The description name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
				
				},

				plugins: {
					trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit()
				}
			}
		);
	}

    var _initDemo2 = function () {
        FormValidation.formValidation(
            document.getElementById('listForm'),
            {
                fields: {
                    Title: {
                        validators: {
                            notEmpty: {
                                message: 'Title is required'
                            },
                            regexp: {
                                regexp: /^[a-zs]+$/i,
                                message: 'The full name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    Description: {
                        validators: {
                            notEmpty: {
                                message: 'Description is required'
                            },
                            regexp: {
                                regexp: /^[a-zs]+$/i,
                                message: 'The full name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                   
                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    submitButton: new FormValidation.plugins.SubmitButton()
                }
            }
        );
    }

    var _initDemo3 = function () {
        FormValidation.formValidation(
            document.getElementById('taskForm'),
            {
                fields: {
                    Title: {
                        validators: {
                            notEmpty: {
                                message: 'Title is required'
                            },
                            regexp: {
                                regexp: /^[a-zs]+$/i,
                                message: 'The full name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },
                    Description: {
                        validators: {
                            notEmpty: {
                                message: 'Description is required'
                            },
                            text: {
                                message: 'The value is not a valid for Description'
                            }
                        }
                    },
                    RefTitle: {
                        validators: {
                            regexp: {
                                regexp: /^[a-zs]+$/i,
                                message: 'The full name can consist of alphabetical characters and spaces only'
                            }
                        }
                    },

                    TRefUrl: {
                        validators: {
                            notEmpty: {
                                message: 'Website URL is required'
                            },
                            uri: {
                                message: 'The website address is not valid'
                            }
                        }
                    },

           
                },

                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    submitButton: new FormValidation.plugins.SubmitButton()
                }
            }
        );
    }

	return {
		// public functions
		init: function() {
			_initDemo1();
            //_initDemo2();
            //_initDemo3();
		}
	};
}();

jQuery(document).ready(function() {
	KTFormControls.init();
});

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExcelTask.Startup))]
namespace ExcelTask
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
